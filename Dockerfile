# a docker image that run jupyterhub
FROM registry.gitlab.com/surf.mherkazandjian/testsurfsrccomponentjupyterhub:latest

ENTRYPOINT [ "/usr/local/bin/jupyterhub", "-f", "/etc/jupyterhub/jupyterhub_config.py" ]

EXPOSE 8000
